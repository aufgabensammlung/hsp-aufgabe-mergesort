using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using MergeSort_Array;

namespace MergeSort_Array.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // Arrange
            Program prg = new Program();
            

            //Act
            var result = prg.MergeSort(new int[] { 12, 31, 68, 54 });

            //Assert
            Assert.IsTrue(Enumerable.SequenceEqual(new int[] { 12, 31, 54, 68 }, result));
        }


        [TestMethod]
        public void TestMethod2()
        {
            // Arrange
            Program prg = new Program();

            //Act
            var result = prg.MergeSort(new int[] { 12, 31, 68, 54, 13, 56, 43, 87, 3, 9, 59, 76 });

            //Assert
            Assert.IsTrue(Enumerable.SequenceEqual(new int[] { 3, 9, 12, 13, 31, 43, 54, 56, 59, 68, 76, 87 }, result));
        }
    }
}

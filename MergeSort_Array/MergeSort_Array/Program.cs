﻿using System;

namespace MergeSort_Array
{
    public class Program
    {


        public int[] Merge(int[] linksArray, int[] rechtsArray)     //sollen studenten schreiben
        {

            int resultlength = rechtsArray.Length + linksArray.Length;
            int[] result = new int[resultlength];

            // Ergänzen sie Ihren Code hier 
            // Geschrieben werden muss der "Merge" Algorithmus bei dem die zwei Arrays zu einem Sortiert werden












            return result;
        }




        static void Main(string[] args)
        {
            Program prg = new Program();

            int[] unsortiert = new int[10];
           

            Random random = new Random();


            Console.WriteLine("Anfangs Array:");
            for (int i = 0; i<unsortiert.Length; i++)
            {
                unsortiert[i] = random.Next(1,100);            
                Console.Write(unsortiert[i] + " ");
            }
            Console.WriteLine("\n");

            int[] sortiert = new int[unsortiert.Length];
            sortiert = prg.MergeSort(unsortiert);

            Console.WriteLine("Sortiertes Array:");
            foreach (int x in sortiert)
            {
                Console.Write(x + " ");
            }
            Console.Write("\n");
        }


        public int[] MergeSort(int[] unsortiert)
        {
            if (unsortiert.Length <= 1)
                return unsortiert;

            int[] linksArray;
            int[] rechtsArray;
            int[] result = new int[unsortiert.Length];
            int x = 0;

            int mitte = unsortiert.Length / 2;
            linksArray = new int[mitte];

            if (unsortiert.Length % 2 == 0)
                rechtsArray = new int[mitte];
           
            else
                rechtsArray = new int[mitte + 1];
            

            for (int i = 0; i < mitte; i++)  //Teilen der Unsortierten Liste
            {
                linksArray[i] = unsortiert[i];                             
            }
            for (int i = mitte; i < unsortiert.Length; i++)
            {
                rechtsArray[x] = unsortiert[i];
                x++;
            }

            linksArray = MergeSort(linksArray);
            rechtsArray = MergeSort(rechtsArray);
            result = Merge(linksArray, rechtsArray);
            return result;
        }








    }

}


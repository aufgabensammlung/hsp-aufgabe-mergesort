# Mergesort
Aufgabe Mergesort für Hochsprachenprogrammierung / Mecke an der Jade Hochschule

Aufgabe: Implementieren Sie den Mergesort Algorithmus in der Methode "Merge" innerhalb der Klasse "Program" MergeSort_Array\Program.cs

Folgende Schritte sind für die Iplementierung [des Algorithmus](http://www.sorting-algorithms.com/) notwendig:
* die wird einmal in dem Folgenden Video Ausführlich beschrieben


## Video

https://user-images.githubusercontent.com/72017165/117287043-32624700-ae6a-11eb-9f52-8ccfafb16586.mp4


## Getting Started
* Clonen Sie die Repository auf Ihren Rechner
* Öffnen Sie die Solution *.sln in Visual Studio
* Führen Sie die Implementierung in der Methode durch

Wenn Sie fertig sind, dann
* Im Projektmappen Explorer (engl. Solution Explorer) klicken Sie rechts das -Unittesting Projekt und wählen Sie "Tests ausführen"
* Starten Sie die Tests innerhalb des Test-Explorers

Diesen Ablauf können Sie auch mit Hilfe des Videos "Aufgabensammlung Einstieg" nachvollziehen
